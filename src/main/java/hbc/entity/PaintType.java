package hbc.entity;

/**
 * The two types of paint,
 */
public enum PaintType {
    M("MATTE"),
    G("GLOSS");

    PaintType(final String fullName) {
        this.fullName = fullName;
    }

    // More easy to reason about for developers not familiar with the enum
    private final String fullName;
}
