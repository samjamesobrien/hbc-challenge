package hbc.entity;

import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * Describe the parsed file input as an object.
 */
@Data
public class ParsedInput {

    private Integer numberOfColours;

    private List<CustomerPreferences> customerPreferencesList;

    public ParsedInput() {
        this.customerPreferencesList = new LinkedList<>();
    }
}
