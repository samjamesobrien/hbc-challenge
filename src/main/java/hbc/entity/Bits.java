package hbc.entity;

import lombok.Getter;

import java.util.BitSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * The length of {@link BitSet#length()} or {@link BitSet#size()} isn't reliable for all 0's, we want to store the desired length.
 */
@Getter
public class Bits extends BitSet {

    private final int fixedLength;

    public Bits(int fixedLength) {
        super(fixedLength);
        this.fixedLength = fixedLength;
    }

    @Override
    public String toString() {
        return IntStream.range(0, fixedLength).boxed()
                .map(i -> get(i) ? 1 : 0)
                .collect(Collectors.toList())
                .toString();
    }
}
