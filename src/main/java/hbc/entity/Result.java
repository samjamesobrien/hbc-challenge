package hbc.entity;

import lombok.Setter;

import java.util.BitSet;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Describe the result of the solver.
 */
@Setter
public class Result {

    private static final String FAILURE = "No solution exists";

    private boolean succeeded;

    private List<PaintType> paintTypeList;

    /**
     * Convert the result to the expected string.
     */
    public String writeResultAsString() {
        return succeeded && paintTypeList != null
                ? paintTypeList.stream().map(Enum::name).reduce((s1, s2) -> s1 + " " + s2).orElse("")
                : FAILURE;
    }
}
