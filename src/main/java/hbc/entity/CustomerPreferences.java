package hbc.entity;

import lombok.Data;

import java.util.*;

/**
 * A customer's preferred colour(s).
 */
@Data
public class CustomerPreferences {

    // It looks like these are ordered in the input files and the output requires ordered results
    // so it makes sense to enforce that
    private final SortedSet<Paint> colourPreferences;

    public CustomerPreferences() {
        this.colourPreferences = new TreeSet<>(Comparator.comparing(Paint::getColourCode));
    }
}
