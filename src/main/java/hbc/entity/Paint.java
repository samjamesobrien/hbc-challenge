package hbc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * An individual paint, with a colour and {@link PaintType}.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Paint {

    private Integer colourCode;

    private PaintType paintType;
}
