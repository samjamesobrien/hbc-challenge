package hbc.service;

import hbc.entity.Paint;
import hbc.entity.ParsedInput;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class ValidationService {

    /**
     * Is the {@link ParsedInput} valid to proceed.
     */
    public boolean validateInput(final ParsedInput input) throws IllegalArgumentException {
        return validateNumberOfColours(input)
                && validateIsSolvable(input);
    }


    /**
     * Ensure the number of colours argument matches the actual colours.
     */
    private boolean validateNumberOfColours(final ParsedInput input) {
        final Integer numberOfColours = input.getNumberOfColours();

        final List<Integer> distinctSortedColourCodes = input.getCustomerPreferencesList().stream()
                .flatMap(colourPreferences -> colourPreferences.getColourPreferences().stream())
                .map(Paint::getColourCode)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        // The number of colours should match
        if (distinctSortedColourCodes.size() != numberOfColours) {
            throw new IllegalArgumentException(
                    "The number of colours in the input file do not match the number given in the first row");
        }

        // The colours should all appear
        IntStream.range(0, numberOfColours).forEach(i -> {
            final Integer colourCode = distinctSortedColourCodes.get(i);
            final Integer expected = (i + 1);
            if (!colourCode.equals(expected)) {
                throw new IllegalArgumentException(
                        "Expected colourCode: " + expected + " got " + colourCode + ", all colours: " + distinctSortedColourCodes);
            }
        });

        return true;
    }

    /**
     * Some inputs are un-solvable and can be quickly determined to be so
     */
    private boolean validateIsSolvable(final ParsedInput input) {

        // todo

        return true;
    }
}
