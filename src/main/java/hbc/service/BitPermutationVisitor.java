package hbc.service;

import hbc.entity.Bits;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

/**
 * For a given size bit set, submit permutations of those bits to a Predicate and return the first match.
 */
@Slf4j
class BitPermutationVisitor {

    private final int bitSetSize;

    private final Predicate<Bits> isSolutionSuitable;

    private Optional<Bits> result;

    /**
     * New visitor, will permute N bits from 0's to 1's until the Predicate passes.
     * @param bitSetSize the number of bits.
     * @param test the predicate that stops the permutations.
     */
    public BitPermutationVisitor(final int bitSetSize, final Predicate<Bits> isSolutionSuitable) {
        this.bitSetSize = bitSetSize;
        this.isSolutionSuitable = isSolutionSuitable;
        this.result = Optional.empty();
    }

    /**
     * Iterate over all permutations of the bits, ordered from least 1's to most,
     * returning the first to match the predicate.
     * <p>As the least 1's result is the "cheapest" we want to stop calculating permutations once a solution is found,
     * by ordering by cheapest first we naturally hit the most desirable result first.</p>
     * @return the first matching result.
     */
    public Optional<Bits> permuteAndTestBits() {

        // The first permutation is all zeros
        final Bits zeros = new Bits(bitSetSize);
        if (isSolutionSuitable.test(zeros)) return Optional.of(zeros);

        Set<Bits> lastGeneratedBitSets = new LinkedHashSet<>();
        lastGeneratedBitSets.add(zeros);

        // Generate permutations in order of numbers of 1's
        for (int i = 0; i < bitSetSize; i++) {
            // If the result is present stop permutation
            if (result.isPresent()) break;

            log.debug("Generating BitSets with {} '1'(s)", (i + 1));
            // Take the last collection of generated permutations as the source for the next set
            lastGeneratedBitSets = generatePermutationsForInputs(lastGeneratedBitSets, isSolutionSuitable);
        }

        return result;
    }

    /**
     * For the collection of BitSets given, use them as a base to generate more permutations with one more 1 value.
     */
    private Set<Bits> generatePermutationsForInputs(
            final Set<Bits> inputs, final Predicate<Bits> isSolutionSuitable) {

        final Set<Bits> generated = new LinkedHashSet<>();

        // Track skipable iterations
        int j = 0;

        // For each base bitset
        outerLoop: for (final Bits input : inputs) {
            // We can skip j iterations as they have been covered in a previous iteration
            for (int i = j; i < bitSetSize; i++) {

                // Has the value at the given index not been 1 yet?
                boolean valueAtIndex = input.get(i);
                if (!valueAtIndex) {
                    // Add a new output
                    final Bits output = (Bits) input.clone();
                    output.set(i, true);
                    generated.add(output);

                    if (log.isDebugEnabled()) {
                        log.debug("Generated BitSet: {}", bitSetToString(output));
                    }

                    // If the bit set passes the predicate, break out of this loop and set the value
                    if (isSolutionSuitable.test(output)) {
                        result = Optional.of(output);
                        log.debug("Found BitSet that is a valid solution");
                        break outerLoop;
                    }
                }
            }

            j++;
        }

        return generated;
    }

    /**
     * BitSet's store the indexes of 1 value bits, we want a nice String of bits.
     */
    private static String bitSetToString(final Bits bitSet) {
        final StringBuilder s = new StringBuilder();
        for (int i = 0; i < bitSet.getFixedLength(); i++) {
            s.append(bitSet.get(i) ? 1 : 0);
        }
        return s.toString();
    }
}
