package hbc.service;

import com.google.inject.Inject;
import hbc.entity.ParsedInput;
import hbc.entity.Result;
import hbc.read.InputReader;
import hbc.write.OutputWriter;

/**
 * Solve the HBC challenge.
 */
public class ChallengeSolverService {

    @Inject
    public ValidationService validationService;

    @Inject
    public SolverService solverService;

    @Inject
    public InputReader reader;

    @Inject
    public OutputWriter writer;

    /**
     * Solve the HBC challenge and write the output.
     * @param fileName the file to use.
     */
    public void solveChallenge(final String fileName) {
        // Get the input file
        final ParsedInput input = reader.readAndParseInput(fileName);
        // Verify it is valid input
        validationService.validateInput(input);
        // Solve
        final Result result = solverService.solve(input);
        // Write
        writer.writeOutput(result);
    }
}
