package hbc.service;

import hbc.entity.*;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

import static hbc.mappers.Mappers.BIT_SET_TO_PAINT_LIST_MAPPER;
import static hbc.mappers.Mappers.BIT_SET_TO_RESULT_MAPPER;

@Slf4j
class SolverService {

    /**
     * Solve the actual challenge.
     */
    public Result solve(final ParsedInput input) {

        final BitPermutationVisitor bitPermutationVisitor =
                new BitPermutationVisitor(input.getNumberOfColours(), b -> isSolutionSatisfactory(input, b));

        // The best result is all Gloss if possible
        final Optional<Bits> bestResult = bitPermutationVisitor.permuteAndTestBits();

        return bestResult
                .map(BIT_SET_TO_RESULT_MAPPER::map)
                .orElseGet(Result::new);
    }

    /**
     * Is the current solution satisfactory.
     */
    private boolean isSolutionSatisfactory(
            final ParsedInput input, final Bits bestResultBits) {

        final List<Paint> bestResult = BIT_SET_TO_PAINT_LIST_MAPPER.map(bestResultBits);

        return allCustomersHaveAtLeastOnePreferredPaint(input, bestResult);
    }

    /**
     * Each customer must have at least one {@link Paint} they like.
     */
    private boolean allCustomersHaveAtLeastOnePreferredPaint(
            final ParsedInput input, final List<Paint> bestResult) {

        return input.getCustomerPreferencesList().stream()
                .allMatch(prefs -> customerHasAtLeastOnePreferredPaint(prefs, bestResult));
    }

    /**
     * Do the current best results match at least one {@link CustomerPreferences}.
     */
    private boolean customerHasAtLeastOnePreferredPaint(
            final CustomerPreferences customerPreferences, final List<Paint> bestResult) {

        return customerPreferences.getColourPreferences().stream()
                .anyMatch(pref -> bestResult.get(pref.getColourCode() - 1).getPaintType().equals(pref.getPaintType()));
    }
}
