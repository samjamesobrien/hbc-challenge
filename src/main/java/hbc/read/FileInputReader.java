package hbc.read;

import hbc.entity.CustomerPreferences;
import hbc.entity.Paint;
import hbc.entity.PaintType;
import hbc.entity.ParsedInput;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;

@Slf4j
class FileInputReader implements InputReader {

    /**
     * Read the input file and parse it to the desired DTO.
     */
    @Override
    public ParsedInput readAndParseInput(final String fileName) {
        log.debug("Reading from file: {}", fileName);
        final File file = getFile(fileName);
        final ParsedInput parsedInput = parse(file);
        log.debug("Parsed input result: {}", parsedInput);
        return parsedInput;
    }

    /**
     * Returns resource files or files as an argument.
     */
    private File getFile(final String fileName) {
        final URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
        if (resource != null) {
            log.debug("Got resource: {}", resource);
            return new File(resource.getPath());
        } else {
            File file = new File(fileName);
            log.debug("Got file from path: {}", file.getAbsolutePath());
            return file;
        }
    }

    /**
     * Take the file and parse it to a DTO.
     */
    private ParsedInput parse(final File file) {
        try {
            List<String> allLines = Files.readAllLines(file.toPath());
            log.debug("Read lines: {}", allLines);

            final Iterator<String> iterator = allLines.iterator();

            // Set the expected number of colours
            final Integer numberOfColours = Integer.parseInt(iterator.next());
            final ParsedInput parsedInput = new ParsedInput();
            parsedInput.setNumberOfColours(numberOfColours);

            // Parse the rest of the lines to customer preferred colours
            while (iterator.hasNext()) {
                final String line = iterator.next();
                final CustomerPreferences customerPreferences = lineToCustomerPreferences(line);
                parsedInput.getCustomerPreferencesList().add(customerPreferences);
            }

            return parsedInput;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Parse a line to the expected {@link CustomerPreferences}.
     */
    private CustomerPreferences lineToCustomerPreferences(final String line) {
        final CustomerPreferences customerPreferences = new CustomerPreferences();

        // Iterate over the substrings
        final String[] strings = line.split(" ");
        for (int i = 0; i < strings.length; i += 2) {
            final Paint paint = new Paint();
            final Integer colourCode = Integer.parseInt(strings[i]);
            final PaintType paintType = PaintType.valueOf(strings[i + 1]);
            paint.setColourCode(colourCode);
            paint.setPaintType(paintType);
            customerPreferences.getColourPreferences().add(paint);
        }

        return customerPreferences;
    }
}
