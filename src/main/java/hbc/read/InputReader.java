package hbc.read;

import com.google.inject.ImplementedBy;
import hbc.entity.ParsedInput;

@ImplementedBy(FileInputReader.class)
public interface InputReader {

    /**
     * Get the {@link ParsedInput}.
     */
    ParsedInput readAndParseInput(String fileName);
}
