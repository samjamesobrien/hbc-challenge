package hbc.write;

import com.google.inject.ImplementedBy;
import hbc.entity.Result;

@ImplementedBy(ConsoleWriter.class)
public interface OutputWriter {

    /**
     * Write the output.
     */
    void writeOutput(final Result result);
}
