package hbc.write;

import hbc.entity.Result;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class ConsoleWriter implements OutputWriter {

    /**
     * Write the output to console out as described in the deliverables.
     */
    public void writeOutput(final Result result) {
        System.out.println(result.writeResultAsString());
    }
}
