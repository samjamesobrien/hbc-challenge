package hbc.mappers;

import hbc.entity.Bits;
import hbc.entity.Paint;
import hbc.entity.PaintType;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BitSetToPaintListMapper implements  Mapper<Bits, List<Paint>> {

    BitSetToPaintListMapper() {
    }

    @Override
    public List<Paint> map(final Bits input) {
        return IntStream.range(0, input.getFixedLength())
                .boxed()
                .map(i -> new Paint(i + 1, input.get(i) ? PaintType.M : PaintType.G))
                .collect(Collectors.toList());
    }
}
