package hbc.mappers;

public class Mappers {

    public static final BitSetToPaintListMapper BIT_SET_TO_PAINT_LIST_MAPPER = new BitSetToPaintListMapper();

    public static final BitSetToResultMapper BIT_SET_TO_RESULT_MAPPER = new BitSetToResultMapper();

}
