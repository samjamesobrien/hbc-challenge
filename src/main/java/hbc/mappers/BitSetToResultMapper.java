package hbc.mappers;

import hbc.entity.Bits;
import hbc.entity.PaintType;
import hbc.entity.Result;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BitSetToResultMapper implements Mapper<Bits, Result> {

    BitSetToResultMapper() {
    }

    @Override
    public Result map(final Bits input) {
        final Result result = new Result();

        if (input != null) {
            result.setSucceeded(true);
            final List<PaintType> paintTypes = IntStream.range(0, input.getFixedLength()).boxed()
                    .map(i -> input.get(i) ? PaintType.M : PaintType.G)
                    .collect(Collectors.toList());

            result.setPaintTypeList(paintTypes);
        }

        return result;
    }
}
