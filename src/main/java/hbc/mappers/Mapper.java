package hbc.mappers;

interface Mapper<T, R> {
    /**
     * Convert an instance of {@link T} to an instance of {@link R}.
     */
    R map(T input);
}
