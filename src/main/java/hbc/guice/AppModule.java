package hbc.guice;

import com.google.inject.AbstractModule;

/**
 * Guice application module.
 */

public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();
    }
}
