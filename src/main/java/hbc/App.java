package hbc;

import com.google.inject.Guice;
import com.google.inject.Injector;
import hbc.guice.AppModule;
import hbc.service.ChallengeSolverService;
import lombok.extern.slf4j.Slf4j;

/**
 * Entry point to application.
 */
@Slf4j
public class App {

    /**
     * Main.
     */
    public static void main(String[] args) {
        // Fail fast
        if (args.length != 1) {
            System.out.println("Expected a single filename as argument");
            System.exit(1);
        }

        // Establish filename
        final String fileName = args[0];
        log.debug("Got single argument: {}", fileName);

        // Inject dependencies
        final Injector injector = Guice.createInjector(new AppModule());
        final ChallengeSolverService challengeSolverService = injector.getInstance(ChallengeSolverService.class);

        // Solve
        challengeSolverService.solveChallenge(fileName);
    }
}
