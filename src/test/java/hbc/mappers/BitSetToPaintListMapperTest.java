package hbc.mappers;

import hbc.entity.Bits;
import hbc.entity.Paint;
import hbc.entity.PaintType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BitSetToPaintListMapperTest {

    private BitSetToPaintListMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new BitSetToPaintListMapper();
    }

    @Test
    void map() {
        final Bits bits = new Bits(5);

        // 0, 1, 0, 1, 0
        bits.set(1);
        bits.set(2);
        bits.set(3);
        bits.clear(2);

        final List<Paint> paints = mapper.map(bits);

        // Number of paints
        assertEquals(5, paints.size(), "We expect 5 paints for 5 bits");

        // Ordering of paints & codes
        assertEquals(1, paints.get(0).getColourCode());
        assertEquals(2, paints.get(1).getColourCode());
        assertEquals(3, paints.get(2).getColourCode());
        assertEquals(4, paints.get(3).getColourCode());
        assertEquals(5, paints.get(4).getColourCode());

        // Values of Matte or Gloss
        assertEquals(PaintType.G, paints.get(0).getPaintType());
        assertEquals(PaintType.M, paints.get(1).getPaintType());
        assertEquals(PaintType.G, paints.get(2).getPaintType());
        assertEquals(PaintType.M, paints.get(3).getPaintType());
        assertEquals(PaintType.G, paints.get(4).getPaintType());
    }
}
