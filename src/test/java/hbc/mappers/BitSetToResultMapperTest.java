package hbc.mappers;

import hbc.entity.Bits;
import hbc.entity.Result;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BitSetToResultMapperTest {


    private BitSetToResultMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new BitSetToResultMapper();
    }

    @Test
    void map() {
        final Bits bits = new Bits(5);

        // 0, 1, 0, 1, 0
        bits.set(1);
        bits.set(2);
        bits.set(3);
        bits.clear(2);

        final Result result = mapper.map(bits);

        assertEquals("G M G M G", result.writeResultAsString());
    }
}
