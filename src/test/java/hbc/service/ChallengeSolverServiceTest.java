package hbc.service;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import hbc.entity.Result;
import hbc.guice.AppModule;
import hbc.write.OutputWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ChallengeSolverServiceTest {

    @Mock
    private OutputWriter outputWriter;

    private ChallengeSolverService challengeSolverService;


    @BeforeEach
    public void setUp() throws Exception {
        // init mocks
        MockitoAnnotations.initMocks(this);

        // bind mocks
        final AbstractModule module = new AppModule(){
            @Override
            protected void configure() {
                super.configure();

                bind(OutputWriter.class).toInstance(outputWriter);
            }
        };

        // Get DI instance of class to test
        final Injector injector = Guice.createInjector(module);
        challengeSolverService = injector.getInstance(ChallengeSolverService.class);
    }

    @Test
    public void testSolveChallenge1() {
        testFileAndOutput("test1.txt", "G G G G M");
    }

    @Test
    public void testSolveChallenge2() {
        testFileAndOutput("test2.txt", "No solution exists");
    }

    @Test
    public void testSolveChallenge3() {
        testFileAndOutput("test3.txt", "G M G M G");
    }

    @Test
    public void testSolveChallenge4() {
        testFileAndOutput("test4.txt", "M M");
    }

    @Test()
    public void testSolveChallenge5() {
        assertThrows(java.lang.IllegalArgumentException.class, () ->
                testFileAndOutput("test5.txt", null)
        );
    }


    private void testFileAndOutput(final String fileName, final String expectedResult) {
        assertNotNull(outputWriter);
        assertNotNull(challengeSolverService);

        challengeSolverService.solveChallenge(fileName);

        final ArgumentCaptor<Result> captor = ArgumentCaptor.forClass(Result.class);
        verify(outputWriter, times(1)).writeOutput(captor.capture());
        final Result result = captor.getValue();

        assertNotNull(result);
        assertEquals(expectedResult, result.writeResultAsString());
    }
}
