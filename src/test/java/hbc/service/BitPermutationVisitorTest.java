package hbc.service;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BitPermutationVisitorTest {


    @Test
    void permuteAndTestBits() {
        testPermutations(1, 2);
        testPermutations(2, 4);
    }

    private void testPermutations(final int size, final int expected) {
        final AtomicInteger i = new AtomicInteger(0);
        final BitPermutationVisitor bitPermutationVisitor = new BitPermutationVisitor(size, (bits -> {
            i.incrementAndGet();
            return false;
        }));
        bitPermutationVisitor.permuteAndTestBits();
        assertEquals(expected, i.get(), expected + " permutations should have been visited");
    }
}
