package hbc.service;

import com.google.common.collect.ImmutableList;
import hbc.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SolverServiceTest {

    private SolverService solverService;

    @BeforeEach
    void setUp() {
        solverService = new SolverService();
    }

    @Test
    void solve() {
        // Setup
        final ParsedInput parsedInput = new ParsedInput();
        parsedInput.setNumberOfColours(2);

        final CustomerPreferences customer1 = new CustomerPreferences();
        customer1.getColourPreferences().add(new Paint(1, PaintType.G));

        final CustomerPreferences customer2 = new CustomerPreferences();
        customer2.getColourPreferences().add(new Paint(2, PaintType.M));

        parsedInput.setCustomerPreferencesList(Arrays.asList(customer1, customer2));

        // Test
        final Result result = solverService.solve(parsedInput);

        assertNotNull(result);
        assertEquals("G M", result.writeResultAsString());
    }
}
