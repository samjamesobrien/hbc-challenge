# Paint solver

A lovely solver for managing who gets what paint.

Converts customer preferences to BitSet's and permutes from all 0's to all 1's until a satisfactory solution is found, or not.

## Getting started

To just run it all to verify it works: `./run.sh`

To build: `mvn package`

To run: `java -jar target/hbc-1.0-SNAPSHOT.jar input1.txt` or whatever file you like
